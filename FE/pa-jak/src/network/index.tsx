import axios from 'axios';

export function post(path: () => string, body: () => any): string {
  const BASE_URL = () =>
    'https://cors-anywhere.herokuapp.com/http://159.65.72.49:80/';
  let linkPost: string[] = [];
  for (let key in body()) {
    linkPost.push(key + '=' + body()[key]);
  }
  const result: string = BASE_URL() + path() + '?' + linkPost.join('&');
  console.log(result);
  let dataResult = document.getElementById('data-res') as HTMLElement;
  let resultRes: string = 'loading...';
  axios.post(result).then(
    (response: any) => {
      resultRes = response.data.pajak;
      console.log(resultRes);
      dataResult.innerHTML = resultRes;
    },
    (error: any) => {
      console.log(error);
      resultRes = error;
    }
  );
  return resultRes;
}
