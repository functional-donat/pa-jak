import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-scroll';
import { DropDown, Field, HeaderHalf, MutedText } from 'components/field';
import ResultModal from 'components/result-modal';
import { Container } from 'react-bootstrap';
import { post } from 'network';
import './index.css';
import PpnbmLogo from 'images/rsz_ppnbm.png';

const data = { harga: '', tipe: 'KendaraanBermotor', golongan: '' };

const KategoriBarangTitle = () => {
  return 'Kategori barang?';
};

const KategoriBarang = () => {
  return ['Kendaraan Bermotor', 'Non Kendaraan Bermotor'];
};

const KategoriBarangListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  data['tipe'] = value.replace(/\s/g, '');
};

const GolonganTitle = () => {
  return 'Golongan?';
};

const GolonganListener = (e: any) => {
  data['golongan'] = e.target.value;
};

const GolonganMuted = () => {
  return 'Isi dengan angka 1-7 untuk kendaraan bermotor, 1-4 untuk non kendaraan bermotor';
};

const HargaTitle = () => {
  return 'Berapa harga barang?';
};

const HargaListener = (e: any) => {
  data['harga'] = e.target.value;
};

const Padding = styled.div`
  padding-left: 5vh;
  padding-right: 25vh;
`;

const TitlePadding = styled.div`
  padding-top: 5vh;
  padding-left: 25vh;
  padding-bottom: 5vh;
`;

const TextStyle = styled.div`
  font-family: Karla;
  font-style: normal;
  font-size: 3.2vh;
`;

const Header = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-size: 25px;
`;

const Title = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
`;

const SubmitButton = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1.5vh 6vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 5vh 0;
`;

const Logo = () => {
  return PpnbmLogo;
};

const ppnbm = () => {
  return 'Pajak Penjualan atas Barang Mewah';
};

const PPnBM = () => {
  return 'PPnBM';
};

const urlPost = (): string => {
  return 'ppnbm';
};

const bodyPost = () => {
  return data;
};

const submitForm = () => post(urlPost, bodyPost);

export default function Page() {
  function submit() {
    let modalResult = document.getElementById('result') as HTMLElement;
    let dataResult = document.getElementById('data-res') as HTMLElement;
    if (modalResult.style.display === 'block') {
      modalResult.style.display = 'none';
    } else {
      console.log(bodyPost());
      dataResult.innerHTML = submitForm();
      modalResult.style.display = 'block';
    }
  }

  return (
    <Container>
      <section>
        <TitlePadding>
          <Header>
            <HeaderHalf Logo={Logo} firstTitle={PPnBM} secondTitle={ppnbm} />
          </Header>
          <Title>Menghitung Pajak Penjualan atas Barang Mewah</Title>
          <Padding>
            <TextStyle>
              <Container>
                <DropDown
                  title={KategoriBarangTitle}
                  item={KategoriBarang}
                  handler={KategoriBarangListener}
                />
                <Field
                  title={GolonganTitle}
                  tipe='number'
                  handler={GolonganListener}
                />
                <MutedText mutedText={GolonganMuted} />
                <Field
                  title={HargaTitle}
                  tipe='number'
                  handler={HargaListener}
                />
                <Link to='result'>
                  <SubmitButton onClick={submit}>Submit</SubmitButton>
                </Link>
              </Container>
            </TextStyle>
          </Padding>
        </TitlePadding>
      </section>
      <ResultModal title={() => 'PPnBM'} result={() => '1000000'} />
    </Container>
  );
}
