import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-scroll';
import ResultModal from 'components/result-modal';
import { DropDown, Field, HeaderHalf } from 'components/field';
import { Container } from 'react-bootstrap';
import { post } from 'network';
import './index.css';
import PpnLogo from 'images/rsz_ppn.png';

const data = { tarif_ppn: '', dpp: '' };

const HargaTitle = () => {
  return 'Berapa harga barang / jasa?';
};

const HargaListener = (e: any) => {
  data['dpp'] = e.target.value;
};

const PersenTitle = () => {
  return 'Berapa persen barang / jasa terkena pajak?';
};

const PersenListener = (e: any) => {
  data['tarif_ppn'] = e.target.value;
};

const Padding = styled.div`
  padding-left: 5vh;
  padding-right: 25vh;
`;

const TitlePadding = styled.div`
  padding-top: 5vh;
  padding-left: 25vh;
  padding-bottom: 5vh;
`;

const TextStyle = styled.div`
  font-family: Karla;
  font-style: normal;
  font-size: 3.2vh;
`;

const Header = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-size: 25px;
`;

const Title = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
`;

const SubmitButton = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1.5vh 6vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 5vh 0;
`;

const Logo = () => {
  return PpnLogo;
};

const ppn = () => {
  return 'Pajak Pertambahan Nilai';
};

const Ppn = () => {
  return 'PPN';
};

const urlPost = (): string => {
  return 'ppn';
};

const bodyPost = () => {
  return data;
};

const submitForm = () => post(urlPost, bodyPost);

export default function Page() {
  function submit() {
    let modalResult = document.getElementById('result') as HTMLElement;
    let dataResult = document.getElementById('data-res') as HTMLElement;
    if (modalResult.style.display === 'block') {
      modalResult.style.display = 'none';
    } else {
      console.log(bodyPost());
      dataResult.innerHTML = submitForm();
      modalResult.style.display = 'block';
    }
  }

  return (
    <Container>
      <section>
        <TitlePadding>
          <Header>
            <HeaderHalf Logo={Logo} firstTitle={Ppn} secondTitle={ppn} />
          </Header>
          <Title>Menghitung Pajak Pertambahan Nilai</Title>
          <Padding>
            <TextStyle>
              <Container>
                <Field
                  title={HargaTitle}
                  tipe='number'
                  handler={HargaListener}
                />
                <Field
                  title={PersenTitle}
                  tipe='number'
                  handler={PersenListener}
                />
                <Link to='result'>
                  <SubmitButton onClick={submit}>Submit</SubmitButton>
                </Link>
              </Container>
            </TextStyle>
          </Padding>
        </TitlePadding>
      </section>
      <ResultModal title={() => 'PPN'} result={() => '1000000'} />
    </Container>
  );
}

export function rangePersen() {
  return 1;
}
