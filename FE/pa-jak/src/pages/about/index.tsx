import React from 'react';
import MemberCard from 'components/member-card';
import GitlabLogo from 'images/gitlab.png';
import PhotoAgnes from 'images/agnes.png';
import PhotoAma from 'images/ama.png';
import PhotoArdho from 'images/ardho.png';
import PhotoFauzan from 'images/fauzan.png';
import PhotoFeriyal from 'images/feriyal.png';
import PhotoHira from 'images/hira.png';
import styled from 'styled-components';

const Title = styled.div`
  margin: 2vh;
  font-weight: 700;
  font-size: 2vh;
  text-align: center;
`;

const Name = styled.div`
  margin: 2vh;
  font-weight: 700;
  font-size: 5vh;
  text-align: center;
`;

const MemberContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 0 18%;
  margin: 2vh 10vh;
`;

const Logo = styled.img`
  width: 12vh;
  height: auto;
`;

const NameContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export default function Page() {
  let allMembers = (): any[][] => {
    return [
      ['Agnes Handoko', 'https://gitlab.com/agnes.handoko', PhotoAgnes],
      ['Ahmad Fauzan A. I.', 'https://gitlab.com/ahmad_fauzan458', PhotoFauzan],
      ['Bunga Amalia K.', 'https://gitlab.com/bungamaku', PhotoAma],
      ['Fakhira Devina', 'https://gitlab.com/hiradevina', PhotoHira],
      [
        'Firriyal Bin Yahya',
        'https://gitlab.com/feriyalbinyahya',
        PhotoFeriyal
      ],
      ['Yusuf Tri Ardho M.', 'https://gitlab.com/rd0', PhotoArdho]
    ];
  };

  return (
    <>
      <Title>This website is created by:</Title>
      <NameContainer>
        <a href='https://gitlab.com/functional-donat/pa-jak/'>
          <Logo src={GitlabLogo} alt={'Gitlab'} />
        </a>
        <Name>Functional Donat</Name>
      </NameContainer>
      <MemberContainer>
        {allMembers().map(member => {
          return (
            <MemberCard
              name={() => member[0]}
              gitlab={() => member[1]}
              photo={() => member[2]}
            ></MemberCard>
          );
        })}
      </MemberContainer>
    </>
  );
}
