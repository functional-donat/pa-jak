import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import Graph from 'components/graph';
import axios from 'axios';

export default function Page() {
  // state for result
  const [retTipe, setRetTipe] = useState('');
  const [retNewData, setRetNewData] = useState('');

  // form
  const {
    value: tipePajak,
    bind: bindTipePajak,
    reset: resetTipePajak
  } = useInput('');
  const {
    value: akhirTahun,
    bind: bindAkhirTahun,
    reset: resetAkhirTahun
  } = useInput('');

  const handleSubmit = (evt: any) => {
    evt.preventDefault();
    const form = {
      tipe_pajak: 'PPN',
      tahun_awal: '2020',
      tahun_akhir: akhirTahun
    };
    const BASE_URL = () =>
      'https://cors-anywhere.herokuapp.com/http://159.65.72.49:80/tax-prediction';
    let linkPost: string[] = [
      'tipe_pajak=' + form.tipe_pajak.toUpperCase(),
      'tahun_awal=' + form.tahun_awal,
      'tahun_akhir=' + form.tahun_akhir
    ];
    const result: string = BASE_URL() + '?' + linkPost.join('&');
    console.log(result);

    axios.post(result).then(
      (response: any) => {
        let res = response.data.prediction;
        let str = '';
        for (let i = 0; i <= response.data.prediction.length; i++) {
          str += res[i];
          if (i != response.data.prediction.length) {
            str += ';';
          }
        }
        setRetNewData(str);
        setRetTipe(tipePajak);
        console.log('NEW DATA DI PAGE ->');
        console.log(retNewData);
      },
      (error: any) => {
        console.log(error);
      }
    );
    resetAkhirTahun();
    resetTipePajak();
  };
  return (
    <Container>
      <form onSubmit={handleSubmit}>
        <label>
          Tipe Pajak
          <input type='text' {...bindTipePajak} />
        </label>
        <label>
          Akhir Tahun:
          <input type='text' {...bindAkhirTahun} />
        </label>
        <div>
          <label>Isi Tipe Pajak dengan PPN atau PPH atau PBB (Case Sensitive)</label>
        </div>
        <br/>
        <div>
          <input type='submit' value='Submit' />
        </div>
      </form>
      <section>
        <Graph tipe_pajak={() => retTipe} new_data={() => retNewData}></Graph>
      </section>
    </Container>
  );
}

export const useInput = (initialValue: any) => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(''),
    bind: {
      value,
      onChange: (event: any) => {
        setValue(event.target.value);
      }
    }
  };
};
