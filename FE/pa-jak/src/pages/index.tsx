import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Landing from 'pages/landing';
import Calculate from 'pages/calculate';
import PphForm from 'pages/pph';
import PbbForm from 'pages/pbb';
import PkbForm from 'pages/pkb';
import PpnForm from 'pages/ppn';
import PpnbmForm from 'pages/ppnbm';
import Graph from 'pages/graph';
import About from 'pages/about';
import NavBar from 'components/navbar';

export default function Page() {
  return (
    <>
      <NavBar names={() => ['CALCULATE', 'GRAPH', 'ABOUT']} />
      <Switch>
        <Route exact path='/' component={Landing} />
        <Route path='/calculate' component={Calculate} />
        <Route path='/pph' component={PphForm} />
        <Route path='/pbb' component={PbbForm} />
        <Route path='/pkb' component={PkbForm} />
        <Route path='/ppn' component={PpnForm} />
        <Route path='/ppnbm' component={PpnbmForm} />
        <Route path='/graph' component={Graph} />
        <Route path='/about' component={About} />
      </Switch>
    </>
  );
}
