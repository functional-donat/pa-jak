import React from 'react';
import CalculateCard from 'components/calculate-card';
import PPhLogo from 'images/pph.png';
import PBBLogo from 'images/pbb.png';
import PPNLogo from 'images/ppn.png';
import PPnBMLogo from 'images/ppnbm.png';
import PKBLogo from 'images/pkb.png';
import styled from 'styled-components';

const CalculateContainer = styled.div`
  display: flex;
  margin: 15vh 10vh;
`;

export default function Page() {
  let allPajak = (): any[][] => {
    return [
      ['PPh', 'Penghasilan', PPhLogo],
      ['PBB', 'Bumi dan Bangunan', PBBLogo],
      ['PPN', 'Pertambahan Nilai', PPNLogo],
      ['PPnBM', 'Penjualan atas Barang Mewah', PPnBMLogo],
      ['PKB', 'Kendaraan Bermotor', PKBLogo]
    ];
  };

  return (
    <>
      <CalculateContainer>
        {allPajak().map(pajak => {
          return (
            <CalculateCard
              title={() => pajak[0]}
              desc={() => 'Hitung Pajak ' + pajak[1]}
              logo={() => <img src={pajak[2]} alt={'Logo ' + pajak[0]} />}
            ></CalculateCard>
          );
        })}
      </CalculateContainer>
    </>
  );
}
