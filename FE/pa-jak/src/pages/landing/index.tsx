import React from 'react';
import styled from 'styled-components';
import './index.css';

const TitleContainer = styled.div`
  font-weight: bold;
  text-align: center;
  line-height: 10vh;
  margin: 30vh 8vh;
`;

const Title = styled.div`
  font-size: 8vh;
  color: #c1a032;
`;

const Subtitle = styled.div`
  font-size: 4vh;
`;

export default function Page() {
  return (
    <TitleContainer>
      <Title>PA-JAK</Title>
      <Subtitle>Calculate your taxes</Subtitle>
    </TitleContainer>
  );
}
