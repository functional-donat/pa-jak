import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-scroll';
import { Field, HeaderHalf } from 'components/field';
import ResultModal from 'components/result-modal';
import { Container } from 'react-bootstrap';
import { post } from 'network';
import './index.css';
import PBBLogo from 'images/rsz_pbblogo.png';

const data = {
  luas_tanah: '',
  harga_tanah_per_satuan_luas: '',
  luas_bangunan: '',
  harga_bangunan_per_satuan_luas: ''
};

const luasTanah = () => {
  return 'Luas Tanah';
};

const luasBangunan = () => {
  return 'Luas Bangunan';
};

const hargaTanah = () => {
  return 'Harga Tanah per Meter Persegi';
};

const hargaBangunan = () => {
  return 'Harga Bangunan per Meter Persegi';
};

const pbb = () => {
  return 'Pajak Bumi Bangunan';
};

const PBB = () => {
  return 'PBB';
};

const Logo = () => {
  return PBBLogo;
};

const Padding = styled.div`
  padding-left: 5vh;
  padding-right: 25vh;
`;

const TitlePadding = styled.div`
  padding-top: 5vh;
  padding-left: 25vh;
  padding-bottom: 5vh;
`;

const Title = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
`;

const Header = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-size: 25px;
`;

const TextStyle = styled.div`
  font-family: Karla;
  font-style: normal;
  font-size: 3.2vh;
`;

const SubmitButton = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1.5vh 6vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 5vh 0;
`;

const luasTanahListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  data['luas_tanah'] = value;
};

const hargaTanahListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  data['harga_tanah_per_satuan_luas'] = value;
};

const luasBangunanListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  data['luas_bangunan'] = value;
};

const hargaBangunanListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  data['harga_bangunan_per_satuan_luas'] = value;
};

const urlPost = (): string => {
  return 'pbb';
};

const bodyPost = () => {
  return data;
};

const submitForm = () => post(urlPost, bodyPost);

export default function Page() {
  function submit() {
    let modalResult = document.getElementById('result') as HTMLElement;
    let dataResult = document.getElementById('data-res') as HTMLElement;
    if (modalResult.style.display === 'block') {
      modalResult.style.display = 'none';
    } else {
      console.log(bodyPost());
      dataResult.innerHTML = submitForm();
      modalResult.style.display = 'block';
    }
  }

  return (
    <Container>
      <section>
        <TitlePadding>
          <Header>
            <HeaderHalf Logo={Logo} firstTitle={PBB} secondTitle={pbb} />
          </Header>
          <Title>Data rumah</Title>
          <Padding>
            <TextStyle>
              <Container>
                <Field title={luasTanah} handler={luasTanahListener} />
                <Field title={hargaTanah} handler={hargaTanahListener} />
                <Field title={luasBangunan} handler={luasBangunanListener} />
                <Field title={hargaBangunan} handler={hargaBangunanListener} />
                <Link to='result'>
                  <SubmitButton onClick={submit}>Submit</SubmitButton>
                </Link>
              </Container>
            </TextStyle>
          </Padding>
        </TitlePadding>
      </section>
      <ResultModal title={() => 'PBB'} result={() => '1000000'} />
    </Container>
  );
}
