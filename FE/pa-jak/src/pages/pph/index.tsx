import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-scroll';
import {
  DropDown,
  Field,
  MutedText,
  FieldHalf,
  HeaderHalf
} from 'components/field';
import ResultModal from 'components/result-modal';
import { Container } from 'react-bootstrap';
import { post } from 'network';
import './index.css';
import PPHLogo from 'images/rsz_pphlogo.png';

const data = {
  status_npwp: 'False',
  status_nikah: 'False',
  jumlah_tanggungan: '0',
  penghasilan_tahunan: '0',
  tunjangan_istri: '0',
  tunjangan_anak: '0',
  tunjangan_transport: '0',
  tunjangan_pendidikan_anak: '0',
  uang_pengganti_obat: '0',
  tunjangan_lembur: '0',
  asuransi: '0',
  thr: '0'
};

// this code use pure function
// https://blog.logrocket.com/pure-functional-components-in-react-16-6/
// because
// its return value is only determined by its input values
// its return value is always the same for the same input values
const NPWPTitle = () => {
  return 'Apakah Anda memiliki NPWP?';
};

const TunjanganTransporTitle = () => {
  return 'Tunjangan Transpor';
};

const TunjanganLemburTitle = () => {
  return 'Tunjangan Lembur';
};

const NPWPListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  data['status_npwp'] = value === 'Ya' ? 'True' : 'False';
};

const StatusListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  data['status_nikah'] = value === 'Menikah' ? 'True' : 'False';
};

const TanggunganListener = (e: any) => {
  data['jumlah_tanggungan'] = e.target.value;
};

const TunjanganIstriListener = (e: any) => {
  data['tunjangan_istri'] = e.target.value;
};

const TunjanganPendidikanListener = (e: any) => {
  data['tunjangan_pendidikan_anak'] = e.target.value;
};

const TunjanganTransporiListener = (e: any) => {
  data['tunjangan_transport'] = e.target.value;
};

const TunjanganLemburListener = (e: any) => {
  data['tunjangan_lembur'] = e.target.value;
};

const TunjanganAnakListener = (e: any) => {
  data['tunjangan_anak'] = e.target.value;
};

const TunjanganObatListener = (e: any) => {
  data['uang_pengganti_obat'] = e.target.value;
};

const GajiListener = (e: any) => {
  data['penghasilan_tahunan'] = e.target.value;
};

const AsuransiListener = (e: any) => {
  data['asuransi'] = e.target.value;
};

const THRListener = (e: any) => {
  data['thr'] = e.target.value;
};

const NPWPList = () => {
  return ['Tidak', 'Ya'];
};

const StatusList = () => {
  return ['Belum Menikah', 'Menikah'];
};

const StatusTitle = () => {
  return 'Status Perkawinan';
};

const TanggunganTitle = () => {
  return 'Tanggungan';
};

const TanggunganMuted = () => {
  return '(ex: 5 orang, masukan jumlahnya saja yaitu 5)';
};

const GajiTitle = () => {
  return 'Gaji Pertahun/Uang pensiun';
};

const AsuransiTitle = () => {
  return 'Asuransi Dari Perusahaan';
};

const THRTitle = () => {
  return 'THR';
};

const TunjanganIstriTitle = () => {
  return 'Tunjangan Istri';
};

const TunjanganPendidikanTitle = () => {
  return 'Tunjangan Pendidikan Anak';
};

const TunjanganAnakTitle = () => {
  return 'Tunjangan Anak';
};

const TunjanganObatTitle = () => {
  return 'Uang Pengganti Obat';
};

const Logo = () => {
  return PPHLogo;
};

const pph = () => {
  return 'Pajak Penghasilan';
};

const PPh = () => {
  return 'PPh';
};

const urlPost = (): string => {
  return 'pph';
};

const bodyPost = () => {
  return data;
};

const submitForm = () => post(urlPost, bodyPost);

const Padding = styled.div`
  padding-left: 5vh;
  padding-right: 25vh;
`;

const TitlePadding = styled.div`
  padding-top: 5vh;
  padding-left: 25vh;
  padding-bottom: 5vh;
`;
const Title = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
`;
const TextStyle = styled.div`
  font-family: Karla;
  font-style: normal;
  font-size: 3.2vh;
`;

const Header = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-size: 25px;
`;

const SubmitButton = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1.5vh 6vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 5vh 0;
`;

export default function Page() {
  function submit() {
    let modalResult = document.getElementById('result') as HTMLElement;
    let dataResult = document.getElementById('data-res') as HTMLElement;
    if (modalResult.style.display === 'block') {
      modalResult.style.display = 'none';
    } else {
      console.log(bodyPost());
      dataResult.innerHTML = submitForm();
      modalResult.style.display = 'block';
    }
  }

  return (
    <Container>
      <section>
        <TitlePadding>
          <Header>
            <HeaderHalf Logo={Logo} firstTitle={PPh} secondTitle={pph} />
          </Header>
          <Title>1. Identitas</Title>
          <Padding>
            <TextStyle>
              <Container>
                <DropDown
                  title={NPWPTitle}
                  item={NPWPList}
                  handler={NPWPListener}
                />
                <DropDown
                  title={StatusTitle}
                  item={StatusList}
                  handler={StatusListener}
                />
                <Field
                  title={TanggunganTitle}
                  mutedText={TanggunganMuted}
                  handler={TanggunganListener}
                  tipe='number'
                />
                <MutedText mutedText={TanggunganMuted} />
              </Container>
            </TextStyle>
          </Padding>
        </TitlePadding>
      </section>
      <section>
        <TitlePadding>
          <h3>2. Perhitungan Penghasilan Bruto dalam Setahun</h3>
          <Padding>
            <TextStyle>
              <Container>
                <Field title={GajiTitle} handler={GajiListener} tipe='number' />
                <FieldHalf
                  firstTitle={TunjanganIstriTitle}
                  secondTitle={TunjanganPendidikanTitle}
                  firstHandler={TunjanganIstriListener}
                  secondHandler={TunjanganPendidikanListener}
                  firstType='number'
                  secondType='number'
                />
                <FieldHalf
                  firstTitle={TunjanganAnakTitle}
                  secondTitle={TunjanganObatTitle}
                  firstHandler={TunjanganAnakListener}
                  secondHandler={TunjanganObatListener}
                  firstType='number'
                  secondType='number'
                />
                <FieldHalf
                  firstTitle={TunjanganTransporTitle}
                  secondTitle={TunjanganLemburTitle}
                  firstHandler={TunjanganTransporiListener}
                  secondHandler={TunjanganLemburListener}
                  firstType='number'
                  secondType='number'
                />
              </Container>
            </TextStyle>
            <TextStyle>
              <Container>
                <Field
                  title={AsuransiTitle}
                  handler={AsuransiListener}
                  tipe='number'
                />
                <Field title={THRTitle} handler={THRListener} tipe='number' />
                <Link to='result'>
                  <SubmitButton onClick={submit}>Submit</SubmitButton>
                </Link>
              </Container>
            </TextStyle>
          </Padding>
          </TitlePadding>
      </section>
      <ResultModal title={() => 'PPH'} result={() => '1000000'} />
    </Container>
  );
}
