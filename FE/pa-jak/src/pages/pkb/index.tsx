import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-scroll';
import { DropDown, Field, MutedText, HeaderHalf } from 'components/field';
import ResultModal from 'components/result-modal';
import { Container } from 'react-bootstrap';
import { post } from 'network';
import './index.css';
import PkbLogo from 'images/pkb2.png';

const data = {
  tipe_kendaraan: 'Motor',
  kendaraan_bermotor_ke: 0,
  nilai_pkb: 0,
  swdkllj: 32000,
  bulan_terlambat: 0
};

const KendaraanKeList = () => {
  return [
    'Non Progresif',
    'Pertama',
    'Kedua',
    'Ketiga',
    'Keempat',
    'Kelima dan seterusnya'
  ];
};

const TipeKendaraanTitle = () => {
  return 'Tipe kendaraan?';
};

const TipeKendaraanList = () => {
  return ['Motor', 'Mobil'];
};

const KendaraanKeTitle = () => {
  return 'Motor/Mobil keberapa? (Untuk Pajak Progresif)';
};

const PKBTitle = () => {
  return 'Nominal PKB (pada STNK)';
};

const SWDKLLJTitle = () => {
  return 'Nominal SWDKLLJ (pada STNK)';
};

const TerlambatBulanTitle = () => {
  return 'Terlambat membayar pajak selama (bulan)?';
};

const TipeKendaraanListener = (e: any) => {
  data['tipe_kendaraan'] = e.target.value;
};

const KendaraanKeListener = (e: any) => {
  e.preventDefault();
  const { value } = e.target;
  let kendaraanKe;
  switch (value) {
    case 'Pertama':
      kendaraanKe = 1;
      break;
    case 'Kedua':
      kendaraanKe = 2;
      break;
    case 'Ketiga':
      kendaraanKe = 3;
      break;
    case 'Keempat':
      kendaraanKe = 4;
      break;
    case 'Kelima dan seterusnya':
      kendaraanKe = 5;
      break;
    default:
      kendaraanKe = 0;
  }
  data['kendaraan_bermotor_ke'] = kendaraanKe;
};

const TerlambatBulanListener = (e: any) => {
  data['bulan_terlambat'] = e.target.value === '' ? 0 : e.target.value;
};

const SWDKLLJListener = (e: any) => {
  data['swdkllj'] = e.target.value === '' ? 32000 : e.target.value;
};

const PKBListener = (e: any) => {
  const { value } = e.target;
  data['nilai_pkb'] = value;
};

const KendaraanKeMuted = () => {
  return 'Jika bukan untuk pajak progresif, tidak perlu diisi';
};

const PKBMuted = () => {
  return '(ex: 5 orang, masukan jumlahnya saja yaitu 5)';
};

const TerlambatBulanMuted = () => {
  return '(Masukkan antara 1 sampai 12. Kosongkan saja jika anda tidak telat membayar pajak)';
};

const Padding = styled.div`
  padding-left: 5vh;
  padding-right: 25vh;
`;

const TitlePadding = styled.div`
  padding-top: 5vh;
  padding-left: 25vh;
  padding-bottom: 5vh;
`;

const TextStyle = styled.div`
  font-family: Karla;
  font-style: normal;
  font-size: 3.2vh;
`;

const Header = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-size: 25px;
`;

const Title = styled.h3`
  font-family: Karla;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
`;

const SubmitButton = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1.5vh 6vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 5vh 0;
`;

const Logo = () => {
  return PkbLogo;
};

const pkb = () => {
  return 'Pajak Kendaraan Bermotor';
};

const Pkb = () => {
  return 'PKB';
};

const urlPost = (): string => {
  return 'pkb';
};

const bodyPost = () => {
  return data;
};

const submitForm = () => post(urlPost, bodyPost);

export default function Page() {
  function submit() {
    let modalResult = document.getElementById('result') as HTMLElement;
    let dataResult = document.getElementById('data-res') as HTMLElement;
    if (modalResult.style.display === 'block') {
      modalResult.style.display = 'none';
    } else {
      console.log(bodyPost());
      dataResult.innerHTML = submitForm();
      modalResult.style.display = 'block';
    }
  }

  return (
    <Container>
      <section>
        <TitlePadding>
          <Header>
            <HeaderHalf Logo={Logo} firstTitle={Pkb} secondTitle={pkb} />
          </Header>
          <Title>Menghitung Pajak Kendaraan Bermotor</Title>
          <Padding>
            <TextStyle>
              <Container>
                <DropDown
                  title={TipeKendaraanTitle}
                  item={TipeKendaraanList}
                  handler={TipeKendaraanListener}
                />
                <DropDown
                  title={KendaraanKeTitle}
                  item={KendaraanKeList}
                  handler={KendaraanKeListener}
                />
                <MutedText mutedText={KendaraanKeMuted} />
                <Field
                  title={TerlambatBulanTitle}
                  tipe='number'
                  handler={TerlambatBulanListener}
                />
                <MutedText mutedText={TerlambatBulanMuted} />
                <Field
                  title={PKBTitle}
                  mutedText={PKBMuted}
                  tipe='number'
                  handler={PKBListener}
                />
                <Field
                  title={SWDKLLJTitle}
                  mutedText={PKBMuted}
                  tipe='number'
                  handler={SWDKLLJListener}
                />
                <Link to='result'>
                  <SubmitButton onClick={submit}>Submit</SubmitButton>
                </Link>
              </Container>
            </TextStyle>
          </Padding>
        </TitlePadding>
      </section>
      <ResultModal title={() => 'PKB'} result={() => '1000000'} />
    </Container>
  );
}
