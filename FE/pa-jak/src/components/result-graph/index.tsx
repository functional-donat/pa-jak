import React from 'react';
import styled from 'styled-components';

const Desc = styled.div`
  font-weight: 500;
  font-size: 2vh;
  text-align: center;
  margin: 10px 40px;
`;

const BackButton = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1vh 4vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
`;

const ModalContainer = styled.div`
  position: absolute;
  font-family: 'Open Sans', sans-serif;
  display: none;
`;

const CardContainer = styled.div`
  width: 330px;
  height: 180px;
  padding: 20px;
  margin: 0 44vh 10vh 44vh;
  background: #022c48;
  box-shadow: 0px 5px 4px rgba(0, 0, 0, 0.05), 0px 0px 4px rgba(0, 0, 0, 0.1);
  border-radius: 2vh;
  text-align: center;
`;

export interface Props {
  tipe: () => string;
  tahun: () => string;
  hasil: () => string;
}

export default function CalculateCard({ tipe, tahun, hasil }: Props) {
  function toggle() {
    let x = document.getElementById('result') as HTMLElement;
    if (x.style.display === 'block') {
      x.style.display = 'none';
    } else {
      x.style.display = 'block';
    }
  }
  return (
    <>
      <ModalContainer id='result'>
        <CardContainer>
          <Desc>
            {'Untuk pajak ' + tipe() + ' pada tahun ' + tahun() + ' bernilai: Rp ' + hasil() + ' milyar'}
          </Desc>
          <BackButton onClick={toggle}>Close</BackButton>
        </CardContainer>
      </ModalContainer>
    </>
  );
}
