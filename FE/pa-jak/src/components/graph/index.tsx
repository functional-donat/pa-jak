import React from 'react';
import CanvasJSReact from '../../assets/canvasjs.react.js';
import styled from 'styled-components';

const BackButton = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1vh 4vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
`;

const CanvasJSChart = CanvasJSReact.CanvasJSChart;
let dataPPN = [
  { x: 2007, y: 154527.0 },
  { x: 2008, y: 209647.0 },
  { x: 2009, y: 193067.0 },
  { x: 2010, y: 230605.0 },
  { x: 2011, y: 277800.0 },
  { x: 2012, y: 337584.6 },
  { x: 2013, y: 384713.5 },
  { x: 2014, y: 409181.6 },
  { x: 2015, y: 423710.82 },
  { x: 2016, y: 412213.5 },
  { x: 2017, y: 480724.6 },
  { x: 2018, y: 564682.4 },
  { x: 2019, y: 655394.9 }
];

let dataPPH = [
  { x: 2007, y: 238431.0 },
  { x: 2008, y: 327498.0 },
  { x: 2009, y: 317615.0 },
  { x: 2010, y: 357045.0 },
  { x: 2011, y: 431122.0 },
  { x: 2012, y: 465069.6 },
  { x: 2013, y: 506442.8 },
  { x: 2014, y: 546180.9 },
  { x: 2015, y: 602308.13 },
  { x: 2016, y: 657162.7 },
  { x: 2017, y: 637859.3 },
  { x: 2018, y: 761200.3 },
  { x: 2019, y: 894448.7 }
];

let dataPBB = [
  { x: 2007, y: 23724.0 },
  { x: 2008, y: 25354.0 },
  { x: 2009, y: 24270.0 },
  { x: 2010, y: 28581.0 },
  { x: 2011, y: 29893.0 },
  { x: 2012, y: 28968.9 },
  { x: 2013, y: 25304.6 },
  { x: 2014, y: 23476.2 },
  { x: 2015, y: 29250.05 },
  { x: 2016, y: 19443.2 },
  { x: 2017, y: 16770.3 },
  { x: 2018, y: 17433.9 },
  { x: 2019, y: 19103.6 }
];

const GraphContainer = styled.div`
  background-color: white;
  border-radius: 2vh;
  margin: 5vh;
  padding: 3vh;
`;

const Title = styled.h1`
  color: #022c48;
  text-align: center;
  margin-bottom: 3vh;
`;

export interface Props {
  tipe_pajak: () => string;
  new_data: () => string;
}

export default function CalculateGraph({ tipe_pajak, new_data }: Props) {
  let data;
  if (tipe_pajak().toUpperCase() == 'PPH') {
    data = dataPPH;
  } else if (tipe_pajak().toUpperCase() == 'PPN') {
    data = dataPPN;
  } else if (tipe_pajak().toUpperCase() == 'PBB') {
    data = dataPBB;
  } else if (tipe_pajak().toUpperCase() == '') {
    data = [{}];
    return (
      <GraphContainer id='result'>
        <Title>Silahkan isi tipe pajak dan tahun akhir.</Title>
      </GraphContainer>
    );
  } else {
    return (
      <GraphContainer id='result'>
        <Title>Tipe pajak tidak diketahui.</Title>
      </GraphContainer>
    );
  }
  console.log('NEW DATA DI COMPONENT ->');
  let newData = new_data().split(';');
  console.log(newData);
  for (let i = 0; i <= newData.length; i++) {
    data.push({ x: 2020 + i, y: parseFloat(newData[i]) });
  }
  let graf = {
    animationEnabled: true,
    axisX: { title: 'Tahun' },
    axisY: {
      title: 'Rupiah (dalam Milyar)',
      includeZero: false
    },
    data: [
      {
        type: 'splineArea',
        dataPoints: data
      }
    ]
  };
  return (
    <GraphContainer id='result'>
      <Title>{tipe_pajak().toUpperCase()} Spline Graph</Title>
      <CanvasJSChart options={graf} />
    </GraphContainer>
  );
}
