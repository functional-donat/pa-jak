import React from 'react';
import styled from 'styled-components';
import Button from 'components/button';
import { Link } from 'react-router-dom';

const CardContainer = styled.div`
  background-color: #022c48;
  width: 32vh;
  min-height: 10vh;
  border-radius: 3vh;
  margin: 2.5vh;
  text-align: center;
`;

const LogoContainer = styled.div`
  margin: 3vh 7vh;
`;

const Title = styled.div`
  font-weight: 700;
  font-size: 5vh;
  text-align: center;
`;

const Desc = styled.div`
  font-weight: 500;
  font-size: 2vh;
  text-align: center;
  margin: 1vh 5vh;
`;

const ButtonContainer = styled.div`
  margin: 2vh 3vh;
  text-align: center;
`;

export interface Props {
  title: () => string;
  desc: () => string;
  logo: () => any;
}

export default function CalculateCard({ title, desc, logo }: Props) {
  return (
    <>
      <CardContainer>
        <LogoContainer>{logo()}</LogoContainer>
        <Title>{title()}</Title>
        <Desc>{desc()}</Desc>
        <ButtonContainer>
          <Link to={'/' + title().toLowerCase()}>
            <Button name={() => 'Hitung ' + title()}></Button>
          </Link>
        </ButtonContainer>
      </CardContainer>
    </>
  );
}
