import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import pajakLogo from 'images/logo.png';

const NavContainer = styled.nav`
  height: 10vh;
  margin: 0;
  display: flex;
  justify-content: space-between;
`;

const NavLink = styled(Link)`
  font-weight: bold;
  font-size: 2vh;
  text-decoration: none;
  margin-left: 10vh;
`;

const LinkContainer = styled.div`
  margin: 4vh 8vh;
`;

const LogoContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 4vh 8vh;
`;

const LogoLink = styled(Link)`
  color: #c1a032;
  font-weight: bold;
  font-size: 3.2vh;
  text-decoration: none;
`;

const Logo = styled.img`
  width: 4vh;
  height: 4vh;
  margin: 0.4vh 2vh;
`;

export interface Props {
  names: () => string[];
}

export default function NavBar({ names }: Props) {
  return (
    <NavContainer>
      <LogoContainer>
        <LogoLink to='/'>PA-JAK</LogoLink>
        <Logo src={pajakLogo} alt='pajak-logo' />
      </LogoContainer>
      <LinkContainer>
        {names().map(link => {
          return <NavLink to={'/' + link.toLowerCase()}>{link}</NavLink>;
        })}
      </LinkContainer>
    </NavContainer>
  );
}
