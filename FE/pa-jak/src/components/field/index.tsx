import React from 'react';
import { Col, Row, Form } from 'react-bootstrap';
import styled from 'styled-components';

const PaddingImg = styled.div`
    padding-right: 1.5vh;
`

type DropDownProps = {
  title: () => String;
  item: () => String[];
  handler?: (e: any) => void;
};

type FieldProps = {
  title: () => String;
  mutedText?: () => String;
  handler?: (e: any) => void;
  tipe?: string;
};

type titleProps = {
  title: () => String;
};

type mutedProps = {
  mutedText: () => String;
};

type FieldHalfProps = {
  firstTitle: () => String;
  secondTitle: () => String;
  firstHandler: (e: any) => void;
  secondHandler: (e: any) => void;
  firstType?: string;
  secondType?: string;
};

export const DropDown = ({ title, item, handler }: DropDownProps) => (
  <aside>
    <Form>
      <Form.Group controlId='pekerjaan'>
        <Form.Label>{title()}</Form.Label>
        <Form.Control as='select' onChange={handler}>
          {item().map((i, index) => {
            return <option> {i} </option>;
          })}
        </Form.Control>
      </Form.Group>
    </Form>
  </aside>
);

export const Field = ({ title, mutedText, handler, tipe }: FieldProps) => (
  <aside>
    <Form.Group>
      <Form.Label>{title()}</Form.Label>
      <Form.Control type={tipe} onChange={handler} />
    </Form.Group>
  </aside>
);

export const Title = ({ title }: titleProps) => (
  <aside>
    <title>{title()}</title>
  </aside>
);

export const MutedText = ({ mutedText }: mutedProps) => (
  <aside>
    <Form.Text className='text-muted'>{mutedText()}</Form.Text>
  </aside>
);

export const FieldHalf = ({
  firstTitle,
  secondTitle,
  firstHandler,
  secondHandler,
  firstType,
  secondType
}: FieldHalfProps) => (
    <aside>
      <Form>
        <Form.Row>
          <Col>
            <Form.Label> {firstTitle()} </Form.Label>
            <Form.Control type={firstType} onChange={firstHandler} />
          </Col>
          <Col>
            <Form.Label> {secondTitle()} </Form.Label>
            <Form.Control type={secondType} onChange={secondHandler} />
          </Col>
        </Form.Row>
      </Form>
    </aside>
  );

export const HeaderHalf = ({
  Logo,
  firstTitle,
  secondTitle
}: HeaderHalfProps) => (
    <aside>
      <Row>
        <PaddingImg>
          <Col md='1'>
            <img src={Logo()} alt='logo' />
          </Col>
        </PaddingImg>
        <Col md='6'>
          <Row>{firstTitle()}</Row>
          <Row>{secondTitle()}</Row>
        </Col>
      </Row>
    </aside>
  );

type HeaderHalfProps = {
  firstTitle: () => String;
  secondTitle: () => String;
  Logo: () => string;
};
