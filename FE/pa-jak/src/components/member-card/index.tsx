import React from 'react';
import styled from 'styled-components';

const CardContainer = styled.div`
  background-color: #022c48;
  width: 32vh;
  height: 22vh;
  border-radius: 3vh;
  margin: 3vh 1vh;
  text-align: center;
`;

const LogoContainer = styled.div`
  margin: 3vh 7vh;
`;

const Title = styled.div`
  margin: 2vh;
  font-weight: 700;
  font-size: 2vh;
  text-align: center;
`;

const Photo = styled.img`
  width: 10vh;
  height: auto;
  border-radius: 50%;
`;

export interface Props {
  name: () => string;
  gitlab: () => string;
  photo: () => any;
}

export default function CalculateCard({ name, gitlab, photo }: Props) {
  return (
    <>
      <CardContainer>
        <LogoContainer>
          <a href={gitlab()}>
            <Photo src={photo()} alt={'Gitlab'} />
          </a>
        </LogoContainer>
        <Title>{name()}</Title>
      </CardContainer>
    </>
  );
}
