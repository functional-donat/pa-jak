import React from 'react';
import styled from 'styled-components';

const ButtonStyle = styled.button`
  font-weight: bold;
  font-size: 2vh;
  background-color: #c1a032;
  border: none;
  padding: 1.5vh 6vh;
  border-radius: 1vh;
  text-align: center;
  text-decoration: none;
  display: inline-block;
`;

export interface Props {
  name: () => string;
}

export default function Button({ name }: Props) {
  return (
    <>
      <ButtonStyle>{name()}</ButtonStyle>
    </>
  );
}
