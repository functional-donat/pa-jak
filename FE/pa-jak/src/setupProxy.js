const proxy = require("http-proxy-middleware");

module.exports = app => {
  app.use(
    ["/api/pph",
    "/api/ppnbm",
    "/api/pbb",
    "/api/ppn",
    "/api/tax-prediction"],
    proxy({
      target: "http://159.65.72.49:80",
      changeOrigin: true,
      pathRewrite: {
        "^/api/pph": "/pph",
        "^/api/ppnbm": "/ppnbm",
        "^/api/pbb": "/pbb",
        "^/api/ppn": "/ppn",
        "^/api/tax-prediction": "/tax-prediction"
      }
    })
  );
};