{-# LANGUAGE DeriveGeneric #-}

module PajakBumiBangunan where

import Value (persen, juta)
import Data.Aeson                           
import GHC.Generics

data Pbb = Pbb {
      pajak :: Double
    } deriving (Generic, Show)

instance ToJSON Pbb where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Pbb

pajakBumiBangunan :: (Fractional a, Ord a) => a -> a -> a -> a -> a
pajakBumiBangunan luasTanah hargaTanahPerSatuanLuas luasBangunan hargaBangunanPerSatuanLuas = nilaiJualKenaPajak * persen 0.5
    where nilaiJualKenaPajak = ((luasTanah * hargaTanahPerSatuanLuas) + (luasBangunan) * (hargaBangunanPerSatuanLuas)) * persen 20