module Value where

persen :: Fractional a => a -> a
persen num = num/100

juta :: Num a => a -> a
juta num = num * 1000000