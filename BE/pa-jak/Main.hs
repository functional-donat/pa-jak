{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad                      (msum)
import Happstack.Server                   ( Response, ServerPart, Method(POST)
                                          , BodyPolicy(..), decodeBody, defaultBodyPolicy
                                          , dir, look, nullConf, ok, simpleHTTP
                                          , toResponse, method, port
                                          )
import Data.Data                          (Data, Typeable)
import Data.Map                           (Map, fromList)
import Data.Aeson                     
import PajakPenghasilan
import PajakPenjualanAtasBarangMewah
import PajakPertambahanNilai
import PajakBumiBangunan
import PajakKendaraanBermotor
import Read
import TaxPrediction

main :: IO ()
main = simpleHTTP nullConf {port = 80} $ handlers

myPolicy :: BodyPolicy
myPolicy = (defaultBodyPolicy "/tmp/" 0 1000 1000)

handlers :: ServerPart Response
handlers =
    do decodeBody myPolicy
       msum [ dir "pph" $ pphPart
            , dir "ppnbm" $ ppnbmPart
            , dir "ppn" $ ppnPart
            , dir "pbb" $ pbbPart
            , dir "pkb" $ pkbPart
            , dir "tax-prediction" $ taxPredictionPart
            ]

pphPart :: ServerPart Response
pphPart =
    do Happstack.Server.method POST
       statusNpwp <- look "status_npwp"
       statusNikah <- look "status_nikah"
       jumlahTanggungan <- look "jumlah_tanggungan"
       penghasilanTahunan <- look "penghasilan_tahunan"
       tunjanganIstri <- look "tunjangan_istri"
       tunjanganAnak <- look "tunjangan_anak"
       tunjanganTransport <- look "tunjangan_transport"
       tunjanganPendidikanAnak <- look "tunjangan_pendidikan_anak"
       uangPenggantiObat <- look "uang_pengganti_obat"
       tunjanganLembur <- look "tunjangan_lembur"
       asuransi <- look "asuransi"
       thr <- look "thr"

       let totalPenghasilan =  readDouble penghasilanTahunan + readDouble tunjanganIstri + readDouble tunjanganAnak + readDouble tunjanganTransport + readDouble tunjanganPendidikanAnak+ readDouble uangPenggantiObat+ readDouble tunjanganLembur+ readDouble asuransi+ readDouble thr

       let totalPajak = PajakPenghasilan.pajakPenghasilan (readBool statusNpwp) (readBool statusNikah) (readInteger jumlahTanggungan) totalPenghasilan

       ok $ toResponse (encode (Pph {PajakPenghasilan.pajak = totalPajak}))

ppnbmPart :: ServerPart Response
ppnbmPart =
    do Happstack.Server.method POST
       harga <- look "harga"
       tipe <- look "tipe"
       golongan <- look "golongan"
       let hargaDouble = read harga :: Double
       let golonganInt = read golongan :: Int
       let totalPajak = pajakPenjualanAtasBarangMewah hargaDouble tipe golonganInt
       ok $ toResponse (encode (Ppnbm {PajakPenjualanAtasBarangMewah.pajak = totalPajak}))

ppnPart :: ServerPart Response
ppnPart =
    do Happstack.Server.method POST
       tarifPpn <- look "tarif_ppn"
       dpp <- look "dpp"
       let tarifPpnDouble = read tarifPpn :: Double
       let dppDouble = read dpp :: Double
       let totalPajak = pajakPertambahanNilai tarifPpnDouble dppDouble
       ok $ toResponse (encode (Ppn {PajakPertambahanNilai.pajak = totalPajak}))

pbbPart :: ServerPart Response
pbbPart =
    do Happstack.Server.method POST
       luasTanah <- look "luas_tanah"
       hargaTanahPerSatuanLuas <- look "harga_tanah_per_satuan_luas"
       luasBangunan <- look "luas_bangunan"
       hargaBangunanPerSatuanLuas <- look "harga_bangunan_per_satuan_luas"
       let totalPajak = pajakBumiBangunan (readDouble luasTanah) (readDouble hargaTanahPerSatuanLuas) (readDouble luasBangunan) (readDouble hargaBangunanPerSatuanLuas)
       ok $ toResponse (encode (Pbb {PajakBumiBangunan.pajak = totalPajak}))

pkbPart :: ServerPart Response
pkbPart =
    do Happstack.Server.method POST
       tipeKendaraan <- look "tipe_kendaraan"
       kendaraanBermotorKe <- look "kendaraan_bermotor_ke"
       nilaiPkb <- look "nilai_pkb"
       swdkllj <- look "swdkllj"
       bulanTerlambat <- look "bulan_terlambat"
       let totalPajak = pajakKendaraanBermotor tipeKendaraan (readInteger kendaraanBermotorKe) (readDouble nilaiPkb) (readDouble swdkllj) (readDouble bulanTerlambat)
       ok $ toResponse (encode (Pkb {PajakKendaraanBermotor.pajak = totalPajak}))

taxPredictionPart :: ServerPart Response
taxPredictionPart =
    do Happstack.Server.method POST
       tipePajak <- look "tipe_pajak"
       tahunAwal <- look "tahun_awal"
       tahunAkhir <- look "tahun_akhir"
       let prediction = getDataManyYear tipePajak (readDouble tahunAwal) (readDouble tahunAkhir)
       ok $ toResponse (encode (TaxPrediction {TaxPrediction.prediction = prediction}))
       