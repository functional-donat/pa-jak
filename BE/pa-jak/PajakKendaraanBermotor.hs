{-# LANGUAGE DeriveGeneric #-}

module PajakKendaraanBermotor where

import Value (persen)
import Data.Aeson                           
import GHC.Generics

data Pkb = Pkb {
      pajak :: Double
    } deriving (Generic, Show)

instance ToJSON Pkb where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Pkb

persenProgresifPkbMotor :: (Fractional a, Integral b) =>  b -> a
persenProgresifPkbMotor motorKe
    | motorKe == 1 = persen 1.5
    | motorKe == 2 = persen 2
    | motorKe == 3 = persen 2.5
    | motorKe == 4 = persen 3
    | motorKe >= 5 = persen 3.5

persenProgresifPkbMobil :: (Fractional a, Integral b) =>  b -> a
persenProgresifPkbMobil mobilKe
    | mobilKe == 1 = persen 2
    | mobilKe == 2 = persen 2.5
    | mobilKe == 3 = persen 3
    | mobilKe == 4 = persen 3.5
    | mobilKe >= 5 = persen 4

persenProgresifPkb :: (Fractional a, Integral b) =>  String -> b -> a
persenProgresifPkb tipeKendaraan kendaraanKe
    | tipeKendaraan == "Motor" = persenProgresifPkbMotor kendaraanKe
    | tipeKendaraan == "Mobil" = persenProgresifPkbMobil kendaraanKe

nilaiJualKendaraanBermotor :: Fractional a => a -> a
nilaiJualKendaraanBermotor pkb = (pkb/2) * 100

dendaPkb :: Fractional a => a -> a -> a
dendaPkb nilaiPkb bulanTerlambat = nilaiPkb * persen 25 * (bulanTerlambat/12)

dendaSwdkllj :: Fractional a => [Char] -> a
dendaSwdkllj tipeKendaraan
    | tipeKendaraan == "Motor" = 32000
    | tipeKendaraan == "Mobil" = 100000

denda :: (Fractional a, Ord a) => [Char] -> a -> a -> a
denda tipeKendaraan nilaiPkb bulanTerlambat
    | bulanTerlambat <= 0 = 0
    | otherwise = dendaPkb nilaiPkb bulanTerlambat + dendaSwdkllj tipeKendaraan

pajakKendaraanBermotorProgresif :: (Fractional a, Ord a, Integral b) => [Char] -> b -> a -> a -> a -> a
pajakKendaraanBermotorProgresif tipeKendaraan kendaraanBermotorKe nilaiPkb swdkllj bulanTerlambat = (persenProgresifPkb tipeKendaraan kendaraanBermotorKe * nilaiJualKendaraanBermotor nilaiPkb) + swdkllj + denda tipeKendaraan nilaiPkb bulanTerlambat

pajakKendaraanBermotorNonProgresif :: (Fractional a, Ord a) => [Char] -> a -> a -> a -> a
pajakKendaraanBermotorNonProgresif tipeKendaraan nilaiPkb swdkllj bulanTerlambat = nilaiPkb + swdkllj + denda tipeKendaraan nilaiPkb bulanTerlambat

-- disini function untuk menghitung pajak secara keseluruhan
pajakKendaraanBermotor :: (Fractional a, Ord a, Integral b) => [Char] -> b -> a -> a -> a -> a
pajakKendaraanBermotor tipeKendaraan kendaraanBermotorKe nilaiPkb swdkllj bulanTerlambat
    | kendaraanBermotorKe == 0 = pajakKendaraanBermotorNonProgresif tipeKendaraan nilaiPkb swdkllj bulanTerlambat
    | otherwise =  pajakKendaraanBermotorProgresif tipeKendaraan kendaraanBermotorKe nilaiPkb swdkllj bulanTerlambat
