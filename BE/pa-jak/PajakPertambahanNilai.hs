{-# LANGUAGE DeriveGeneric #-}

module PajakPertambahanNilai where

import Value (persen)
import Data.Aeson                           
import GHC.Generics

data Ppn = Ppn {
      pajak :: Double
    } deriving (Generic, Show)

instance ToJSON Ppn where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Ppn

pajakPertambahanNilai :: Fractional a => a -> a -> a
pajakPertambahanNilai tarif_ppn dpp = persen tarif_ppn * dpp