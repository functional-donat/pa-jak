module Read where

readDouble :: String -> Double
readDouble str = read str :: Double

readBool :: String -> Bool
readBool str = read str :: Bool

readInteger :: String -> Integer
readInteger str = read str :: Integer