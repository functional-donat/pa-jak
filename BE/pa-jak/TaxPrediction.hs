{-# LANGUAGE DeriveGeneric #-}

module TaxPrediction where

import Value (persen)
import Data.Aeson                           
import GHC.Generics
import Data.List
import Data.PPH
import Data.PBB
import Data.PPN

data TaxPrediction = TaxPrediction {
      prediction :: [Double]
    } deriving (Generic, Show)

instance ToJSON TaxPrediction where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON TaxPrediction

avg :: (Fractional a1, Real a2) => [a2] -> a1
avg xs = realToFrac (sum xs) / genericLength xs

crossDeviation :: (Fractional a2, Real a2) => [a2] -> [a2] -> a2 -> a2
crossDeviation ls1 ls2 n = (sum (zipWith (*) ls1 ls2)) - (n * (avg ls1) * (avg ls2))

-- y = theta0 + theta1 * x 
-- ex: getCoef [1,2,3] [4,5,6] 3
getCoef :: (Fractional a2, Real a2) => [a2] -> [a2] -> a2 -> (a2, a2)
getCoef tahun jumlah size =
    let
        ssXy = crossDeviation tahun jumlah size; 
        ssXx = crossDeviation tahun tahun size
        theta1 = ssXy / ssXx
        theta0 = avg jumlah / (theta1 * avg tahun)
    in (theta0, theta1)

-- y = theta0 + theta1 * x 
theFunc :: Num a => (a, a) -> a -> a
theFunc (theta0, theta1) tahun = theta0 + (theta1)*tahun

-- ex: getData theFunc "PBB" 30
getData :: ((Double, Double) -> t -> p) -> [Char] -> t -> p
getData f tipePajak tahun
    | tipePajak == "PPN" = f (getCoef tahunPpn dataPpn 12) tahun
    | tipePajak == "PPH" = f (getCoef tahunPph dataPph 12) tahun
    | tipePajak == "PBB" = f (getCoef tahunPbb dataPbb 12) tahun

getDataManyYear :: [Char] -> Double -> Double -> [Double]
getDataManyYear tipePajak tahunAwal tahunAkhir = map (getData theFunc tipePajak) [(tahunAwal - 2000)..(tahunAkhir - 2000)]

-- test example
testSample = do
    let size = 10
    let dataX = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] :: [Double]
    let dataY = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90] :: [Double]

    print $ getCoef dataX dataY size

-- test pbb data
testPBB = do
    print $ getCoef tahunPbb dataPbb 12
    print $ getData theFunc "PBB" 30

-- test pph data
testPPH = do
    print $ getCoef tahunPph dataPph 12

-- test ppn data
testPPN = do
    print $ getCoef tahunPpn dataPpn 12
