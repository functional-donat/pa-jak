{-# LANGUAGE DeriveGeneric #-}

module PajakPenghasilan where

import Value (persen, juta)
import Data.Aeson                           
import GHC.Generics

data Pph = Pph {
      pajak :: Double
    } deriving (Generic, Show)

instance ToJSON Pph where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Pph

persenPphDenganNpwp :: (Fractional a, Ord a) => a -> a
persenPphDenganNpwp penghasilanBersih
    | penghasilanBersih > juta 500 = persen 30
    | penghasilanBersih > juta 250 = persen 25
    | penghasilanBersih > juta 50 = persen 15
    | otherwise = persen 5

persenPphTanpaNpwp :: (Fractional a, Ord a) => a -> a
persenPphTanpaNpwp penghasilanBersih = (persenPphDenganNpwp penghasilanBersih) * (persen 120)

persenPph :: (Fractional a, Ord a) => Bool -> (a -> a)
persenPph npwp
    | npwp == True = (\x -> persenPphDenganNpwp x)
    | npwp == False = (\x -> persenPphTanpaNpwp x)

ptkpsn :: (Fractional a, Ord a) => Bool -> a
ptkpsn statusNikah
    | statusNikah == True = juta 4.5
    | otherwise = 0

ptkpjt :: (Fractional a, Ord a) => Integer -> a
ptkpjt jumlahTanggungan
    | jumlahTanggungan == 0 = 0
    | jumlahTanggungan == 1 = juta 4.5
    | jumlahTanggungan == 2 = juta 9
    | jumlahTanggungan >= 3 = juta 13.5

penghasilanTidakKenaPajak :: (Fractional a, Ord a) => Bool -> Integer -> a
penghasilanTidakKenaPajak statusNikah jumlahTanggungan = juta 54 + ptkpsn statusNikah + ptkpjt jumlahTanggungan

penghasilanBersih :: (Fractional a, Ord a) => Bool -> Integer -> a -> a
penghasilanBersih statusNikah jumlahTanggungan penghasilan = penghasilan - ptkp
    where ptkp = penghasilanTidakKenaPajak statusNikah jumlahTanggungan

pajakPenghasilan :: (Fractional a, Ord a) => Bool -> Bool -> Integer -> a -> a
pajakPenghasilan statusNpwp statusNikah jumlahTanggungan penghasilan = 
    let nilaiPenghasilanBersih = penghasilanBersih statusNikah jumlahTanggungan penghasilan
        persentasePph = persenPph statusNpwp
    in pajakPenghasilanHelper nilaiPenghasilanBersih persentasePph

pajakPenghasilanHelper :: (Fractional a, Ord a) => a -> (a -> a) -> a
pajakPenghasilanHelper nilaiPenghasilanBersih persentasePph
    | nilaiPenghasilanBersih > juta 500 = (persentasePph nilaiPenghasilanBersih * (nilaiPenghasilanBersih - juta 500)) + pajakPenghasilanHelper (juta 500) persentasePph
    | nilaiPenghasilanBersih > juta 250 = (persentasePph nilaiPenghasilanBersih * (nilaiPenghasilanBersih - juta 250)) + pajakPenghasilanHelper (juta 250) persentasePph
    | nilaiPenghasilanBersih > juta 50 = (persentasePph nilaiPenghasilanBersih * (nilaiPenghasilanBersih - juta 50)) + pajakPenghasilanHelper (juta 50) persentasePph
    | nilaiPenghasilanBersih <= 0 = 0
    | otherwise = persentasePph nilaiPenghasilanBersih * nilaiPenghasilanBersih