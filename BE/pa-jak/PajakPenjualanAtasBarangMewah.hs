{-# LANGUAGE DeriveGeneric #-}

module PajakPenjualanAtasBarangMewah where

import Value (persen)
import Data.Aeson                           
import GHC.Generics

data Ppnbm = Ppnbm {
      pajak :: Double
    } deriving (Generic, Show)

instance ToJSON Ppnbm where
    toEncoding = genericToEncoding defaultOptions

instance FromJSON Ppnbm

persenPPNBM :: (Eq a, Num a, Fractional b, Ord b) => String -> a -> b
persenPPNBM tipe golongan
    | tipe == "KendaraanBermotor" = persenPPNBMKendaraanBermotor golongan
    | tipe == "NonKendaraanBermotor" = persenPPNBMNonKendaraanBermotor golongan

persenPPNBMKendaraanBermotor :: (Eq a, Num a, Fractional b, Ord b) => a -> b
persenPPNBMKendaraanBermotor golongan
    | golongan == 1 = persen 10
    | golongan == 2 = persen 20
    | golongan == 3 = persen 30
    | golongan == 4 = persen 40
    | golongan == 5 = persen 50
    | golongan == 6 = persen 60
    | golongan == 7 = persen 125

persenPPNBMNonKendaraanBermotor :: (Eq a, Num a, Fractional b, Ord b) => a -> b
persenPPNBMNonKendaraanBermotor golongan
    | golongan == 1 = persen 20
    | golongan == 2 = persen 40
    | golongan == 3 = persen 50
    | golongan == 4 = persen 75

pajakPenjualanAtasBarangMewah :: (Fractional a, Ord a, Eq b, Num b) => a -> String -> b -> a
pajakPenjualanAtasBarangMewah harga tipe golongan = harga * (persenPPNBM tipe golongan)
