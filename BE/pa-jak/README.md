# Install happstack-server

```
$ export PATH=~/.cabal/bin:$PATH
$ cabal update
$ cabal install happstack-server
```

# Install Data.Aeson for JSON Response

```
$ cabal install aeson
```

# Buld Application

```
$ ghc --make -threaded Main.hs -o Main
```

# Run Haskell (Avoid Compilation Step)

```
$ runhaskell Main.hs
```
