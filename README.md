# Pa-jak project

[![Netlify Status](https://api.netlify.com/api/v1/badges/01e2e6b5-5979-4030-bbca-bb1b229f5f6b/deploy-status)](https://app.netlify.com/sites/pa-jak/deploys)

### A website that calculates personal taxes and predicts national taxes trends. Implemented using React, TypeScript, and Haskell.

### https://pa-jak.netlify.com/

### With 5 types of personal taxes:

1. Pajak Penghasilan (PPh)
2. Pajak Bumi dan Bangunan (PBB)
3. Pajak Pertambahan Nilai (PPN)
4. Pajak Kendaraan Bermotor (PKB)
5. Penjualan atas Barang Mewah (PPnBM)

Prediction of national taxes trends (PPN, PPh and PPB) using Linear Regression with Least Square.
The [national taxes data](https://www.bps.go.id/statictable/2009/02/24/1286/realisasi-penerimaan-negara-milyar-rupiah-2007-2019.html) (2007 - 2019) are from Badan Pusat Statistik Indonesia.

### Created by:

1. [Agnes Handoko](https://gitlab.com/agnes.handoko)
2. [Ahmad Fauzan A. I.](https://gitlab.com/ahmad_fauzan458)
3. [Bunga Amalia K.](https://gitlab.com/bungamaku)
4. [Fakhira Devina](https://gitlab.com/hiradevina)
5. [Firriyal bin Yahya](https://gitlab.com/feriyalbinyahya)
6. [Yusuf Tri Ardho](https://gitlab.com/rd0)

### For development:

- To resolve each issue, create new branch with `git checkout -b "[username]/[issue's number]"`, or if the branch is already created just change to that branch with `git checkout "[username]/[issue's number]"`.
- Pull the latest update from branch Master.
- Work on your branch, create a new merge request and assign yourself at the issue that you want to resolve.
- For Front-End development, to run the application on your local machine, go to the folder `FE/pa-jak` and run `npm run start`.
- The preview of the deployed application is available for all merge requests.
- Once your merge request is merged into the Master, you can see the updated application on https://pa-jak.netlify.com/.
- For Back-End development, to run the application on your local machine, go to the folder `BE/pa-jak` and run `runhaskell Main.hs`, the application run on port `80`.

### API Documentation:

https://pajak.docs.apiary.io/
